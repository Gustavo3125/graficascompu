#include "Ankle.h"

Ankle::Ankle()
{
	
}

Ankle::~Ankle()
{
}

void Ankle::draw()
{
	glPushMatrix();
	{
        glColor3f(0.0f, 1.0f, 1.0f);
        glScalef(0.2f,0.5f,0.2f);

		glutSolidCube(1.0);
	}
	glPopMatrix();
}
