//
//  Arm.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include "Arm.h"


Arm::Arm()
{
    shoulder = new Shoulder();
    bicep= new Bicep();
    tricep= new Tricep();
    elbow = new Elbow();
    foreArm= new ForeArm();
    
    radius = 0.6f;
    rot=0.0f;
    bRot=true;
}

Arm::~Arm()
{
    
}

void Arm::draw()
{
    
    glPushMatrix();
    {


        glPushMatrix();
        {
//            glTranslatef(0.3,0.0f,0);
            glRotatef(rot,0,1,0);
            glScalef(0.75, 0.75, 0.75);

            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            shoulder->draw();
        }
        glPopMatrix();
        glPushMatrix();
        {
            glTranslatef(0.28f,0,0);
            //glScalef(0.5, 0.5, 0.5);
            glRotatef(rot,0,1,0);
            glScalef(0.5, 0.5, 0.5);
            
            bicep->draw();
            
        }
        glPopMatrix();
        glPushMatrix();
        {
            glTranslatef(0.55f,0,0);
            //glScalef(0.5, 0.5, 0.5);
            glRotatef(rot,0,1,0);
            glScalef(0.5, 0.5, 0.3);
            tricep->draw();
            
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(0.75,0.0f,0);
            glRotatef(rot,0,1,0);
            glScalef(0.6, 0.6, 0.6);

            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            elbow->draw();
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(0.86,0.2f,0);
            glRotatef(rot,0,1,0);
            glScalef(0.5, 0.5, 0.5);
            
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            foreArm->draw();
        }
        glPopMatrix();
        
      

        
        
    }
    glPopMatrix();
    
    if(rot>20){
        bRot=false;
        
    }
    if(rot<-20){
        bRot=true;
        
    }
    
    if(bRot==true){
        rot+=0.588f;
    }else{
        rot-=0.588f;
    }
    
}
