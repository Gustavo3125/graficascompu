//
//  Arm.h
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif
#include "Blasters.h"
#include "Shoulder.h"
#include "Bicep.h"
#include "Tricep.h"
#include "cElbow.h"
#include "ForeArm.h"
#include "Eye.h"
#include "Hand.h"

class Arm
{
public:
    Arm();    //Constructor
    ~Arm();   //Destroyer
    
    void draw();
    float radius;
    Shoulder* shoulder;
    Bicep* bicep;
    Tricep* tricep;
    Elbow* elbow;
    ForeArm* foreArm;
    
    float rot;
    bool bRot;
};

