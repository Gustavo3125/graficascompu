//
//  Ball.cpp
//  ProyectoGraficasComputacionales
//
//  Created by Daniela Martín on 24/10/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include "Ball.h"
#include "glm.h"
GLMmodel*    ball;

Ball::Ball()
{
    
    radius = 0.6f;
    rotX = 0.0f;
    rotY = 0.0f;
    
   
    glShadeModel(GL_SMOOTH);
    ball = glmReadOBJ("/Users/gustavomendez/Documents/Universidad ITC/Septimo Semestre/Graficas/OpenGL Graficas/ProyectoGraficasComputacionales/ProyectoGraficasComputacionales/pelotaFut.obj");
//   ball = glmReadOBJ("/Users/DanielaMartin/graficascompu/ProyectoGraficasComputacionales/pelotaFut.obj");
    
//                      /Users/gustavomendez/Documents/Universidad ITC/Septimo Semestre/Graficas/OpenGL Graficas/ProyectoGraficasComputacionales/ProyectoGraficasComputacionales/soccerBall.obj");

    // When the model is too big make it fit inside a 1x1x1 cube:
    glmUnitize(ball);
    glmScale(ball, 10.5f);
    
    // When the model doesn't have normals, glm can add them:
     glmFacetNormals(ball); // and then per face
    glmVertexNormals(ball, 45.0f); // first per vertex
   
   

}

Ball::~Ball()
{
}

void Ball::draw()
{
    glPushMatrix();
    {
        glTranslatef(-0.1,-1.2f,0.5);
       // glTranslatef(0, 0.35f, 0.0f);
        glRotatef(rotX, 1, 0, 0);
        glRotatef(rotY, 0, 1, 0);
        glmDraw(ball, GLM_SMOOTH | GLM_TEXTURE);
    }
    glPopMatrix();
    
    rotY += 0.8f;
    if (rotY > 360)
    {
        rotY = 0;
    }
    rotX += 0.2f;
    if (rotX > 360)
    {
        rotX = 0;
    }
    
}
