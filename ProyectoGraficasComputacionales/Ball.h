//
//  Ball.hpp
//  ProyectoGraficasComputacionales
//
//  Created by Daniela Martín on 24/10/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif



class Ball
{
public:
    Ball();    //Constructor
    ~Ball();   //Destroyer
    
    void draw();
    
    float radius;
    float rotX, rotY;
};
