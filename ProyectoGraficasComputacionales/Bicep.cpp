#include "Bicep.h"

Bicep::Bicep()
{
	
}

Bicep::~Bicep()
{
}

void Bicep::draw()
{
	glPushMatrix();
	{
        glColor3f(0.0f, 1.0f, 1.0f);
        glScalef(0.6f,0.5f,0.7f);

		glutSolidCube(1.0);
	}
	glPopMatrix();
}
