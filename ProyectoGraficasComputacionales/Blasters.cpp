//
//  Blasters.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include <stdio.h>

#include "Blasters.h"

Blasters::Blasters()
{
    
}

Blasters::~Blasters()
{
}

void Blasters::draw()
{
    glPushMatrix();
    {
        glColor3f(1, 1, 1);
        quadratic = gluNewQuadric();
        gluCylinder(quadratic,0.1f,0.1f,1.5f,16,16);
    }
    glPopMatrix();
}

