#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif

class Blasters
{
public:
    Blasters();    //Constructor
    ~Blasters();   //Destroyer
    
    void draw();
    
    GLUquadricObj *quadratic;
};
