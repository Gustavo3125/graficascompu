/*
* Sergio Ruiz.
*
* TC3022. Computer Graphics Course.
* Object-Oriented Programming example.
*/

#ifdef __APPLE__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
    #include <math.h>
#else
    #include "freeglut.h"
    #include <stdio.h>
    #include <math.h>
#endif


#include "Player.h"
#include "Ball.h"
#include "Field.h"


Player* myCar;
Player* myCar2;
Player* myCar3;
Player* myCar4;
Player* myCar5;
Player* myCar6;
Player* myCar7;
Player* myCar8;

Ball* myBall;
Field* myField;

long        frames            = 0;
long        time            = 0;
long        timebase        = 0;
int marcador=0;
int marcador2=0;
float        fps                = 0.0f;
bool reset;
bool exitgame;

GLfloat*    mat1_specular; //<---------------------------------------Material 1 - specular
GLfloat*    mat1_diffuse; //<----------------------------------------Material 1 - diffuse
GLfloat*    mat1_ambient; //<----------------------------------------Material 1 - ambient
GLfloat*    mat1_shininess; //<--------------------------------------Material 1 - shininess

GLfloat*    light1_position; //<-------------------------------------Light 1 - location
GLfloat*    light1_diffuse; //<--------------------------------------Light 1 - diffuse
GLfloat*    light1_specular; //<-------------------------------------Light 1 - specular
GLfloat*    light1_ambient; //<--

GLfloat*    mat3_specular; //<---------------------------------------Material 1 - specular
GLfloat*    mat3_diffuse; //<----------------------------------------Material 1 - diffuse
GLfloat*    mat3_ambient; //<----------------------------------------Material 1 - ambient
GLfloat*    mat3_shininess; //<--------------------------------------Material 1 - shininess

GLfloat*    light3_position; //<-------------------------------------Light 1 - location
GLfloat*    light3_diffuse; //<--------------------------------------Light 1 - diffuse
GLfloat*    light3_specular; //<-------------------------------------Light 1 - specular
GLfloat*    light3_ambient; //<--

GLfloat*    mat0_specular;
GLfloat*    mat0_diffuse;
GLfloat*    mat0_shininess;
GLfloat*    light0_position;

GLfloat*    mat2_specular;
GLfloat*    mat2_diffuse;
GLfloat*    mat2_shininess;
GLfloat*    light2_position;
//
char        bufferFPS[50];
float trans;
float transx; // Only declaration. Not initialization.
float trans2;
float trans2x;
float transPx;

float ballpx;
float ballpy;
float ballpz;

bool portero;
float trans2Px;
bool portero2;
bool check=true;
float rot;
float rot2;
float rot2x;
float rotx;
void inCollision();
bool inCollision2();
void displayText( int x, int y, char* txt );

void init()
{
    myCar = new Player();
    myCar2 = new Player();
    myCar3 = new Player();
    myCar4 = new Player();
    myCar5 = new Player();
    myCar6 = new Player();
    myCar7 = new Player();
    myCar8 = new Player();
    trans=-8.0f;
    transx=0.0f;
    transPx=0.0f;
    portero= true;
    portero2=true;
    rotx=0.0f;
    trans2= 12.0f;
    trans2x=0.0f;
    trans2Px=0.0f;
    rot2x=0.0f;
    rot=0.0f;
    rot2=180.0f;
    reset=false;
    exitgame=false;

    ballpx=0.0f;
    ballpy=-3.4f;
    ballpz=3.2f;
    
    myBall = new Ball();
    myField = new Field();
    glEnable( GL_TEXTURE_2D );
    glClearColor ( 0.0f, 0.0f, 0.0f, 0.0f );
    glShadeModel(GL_SMOOTH);
//    glEnable(GL_DEPTH_TEST);            // Enable check for close and far objects.
    glClearColor(0.0, 0.0, 0.0, 0.0);    // Clear the color state.
    glMatrixMode(GL_MODELVIEW);            // Go to 3D mode.
    glLoadIdentity();                    // Reset 3D view matrix.
    
//    ->material 1 begins
    mat1_specular        = new GLfloat[4]; //<------------------------Reserve memory
    mat1_specular[0]    = 1.0f; //<----------------------------------S1r
    mat1_specular[1]    = 1.0f; //<----------------------------------S1g
    mat1_specular[2]    = 1.0f; //<----------------------------------S1b
    mat1_specular[3]    = 1.0f; //<----------------------------------S1a

    mat1_diffuse        = new GLfloat[4]; //<------------------------Reserve memory
    mat1_diffuse[0]        = 0.0f; //<----------------------------------D1r
    mat1_diffuse[1]        = 1.0f; //<----------------------------------D1g
    mat1_diffuse[2]        = 1.0f; //<----------------------------------D1b
    mat1_diffuse[3]        = 1.0f; //<----------------------------------D1a

    mat1_ambient        = new GLfloat[4]; //<------------------------Reserve memory
    mat1_ambient[0]        = 0.1f; //<----------------------------------A1r
    mat1_ambient[1]        = 0.1f; //<----------------------------------A1g
    mat1_ambient[2]        = 0.1f; //<----------------------------------A1b
    mat1_ambient[3]        = 1.0f; //<----------------------------------A1a

    mat1_shininess        = new GLfloat[1]; //<------------------------Reserve memory
    mat1_shininess[0]    = 60.0f; //<---------------------------------material 1 shininess
    //<-material 1 ends

    //->LIGHT 1 begins
    light1_position        = new GLfloat[4]; //<------------------------Reserve memory
    light1_position[0]    =  -5.0f; //<---------------------------------L1x
    light1_position[1]    =  1.0f; //<---------------------------------L1y
    light1_position[2]    =  10.0f; //<---------------------------------L1z
    light1_position[3]    =  0.0f; //<---------------------------------L1w

    light1_specular        = new GLfloat[4]; //<------------------------Reserve memory
    light1_specular[0]    = 1.0f; //<----------------------------------L1Sr
    light1_specular[1]    = 1.0f; //<----------------------------------L1Sg
    light1_specular[2]    = 1.0f; //<----------------------------------L1Sb
    light1_specular[3]    = 1.0f; //<----------------------------------L1Sa

    light1_diffuse        = new GLfloat[4]; //<------------------------Reserve memory
    light1_diffuse[0]    = 0.9f; //<----------------------------------L1Dr
    light1_diffuse[1]    = 0.9f; //<----------------------------------L1Dg
    light1_diffuse[2]    = 1.0f; //<----------------------------------L1Db
    light1_diffuse[3]    = 1.0f; //<----------------------------------L1Da

    light1_ambient        = new GLfloat[4]; //<------------------------Reserve memory
    light1_ambient[0]    = 0.1f; //<----------------------------------L1Ar
    light1_ambient[1]    = 0.1f; //<----------------------------------L1Ag
    light1_ambient[2]    = 0.1f; //<----------------------------------L1Ab
    light1_ambient[3]    = 1.0f; //<----------------------------------L1Aa
    //<-LIGHT 1 ends
    
//    light0_position = new GLfloat[4];
//    light0_position[0] = 5;
//    light0_position[1] = -5;
//    light0_position[2] = 10.0f;
//    light0_position[3] = 1; // POINT LIGHT
//    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    
    mat0_specular = new GLfloat[4];
    mat0_specular[0] = 1.0f;
    mat0_specular[1] = 1.0f;
    mat0_specular[2] = 1.0f;
    mat0_specular[3] = 1.0f;
    
    mat0_diffuse = new GLfloat[4];
    mat0_diffuse[0] = 1.0f;
    mat0_diffuse[1] = 1.0f;
    mat0_diffuse[2] = 1.0f;
    mat0_diffuse[3] = 1.0f;
    
    mat0_shininess = new GLfloat[1];
    mat0_shininess[0] = 100.0f;
    // Configure LIGHT 0:
    // Configure LIGHT 1:
    glLightfv( GL_LIGHT1, GL_POSITION,  light1_position );
    glLightfv( GL_LIGHT1, GL_AMBIENT,   light1_ambient);
    glLightfv( GL_LIGHT1, GL_DIFFUSE,   light1_diffuse);
    glLightfv( GL_LIGHT1, GL_SPECULAR,  light1_specular);
    
    mat3_specular        = new GLfloat[4]; //<------------------------Reserve memory
    mat3_specular[0]    = 1.0f; //<----------------------------------S1r
    mat3_specular[1]    = 1.0f; //<----------------------------------S1g
    mat3_specular[2]    = 1.0f; //<----------------------------------S1b
    mat3_specular[3]    = 1.0f; //<----------------------------------S1a
    
    mat3_diffuse        = new GLfloat[4]; //<------------------------Reserve memory
    mat3_diffuse[0]        = 0.0f; //<----------------------------------D1r
    mat3_diffuse[1]        = 1.0f; //<----------------------------------D1g
    mat3_diffuse[2]        = 0.0f; //<----------------------------------D1b
    mat3_diffuse[3]        = 1.0f; //<----------------------------------D1a
    
    mat3_ambient        = new GLfloat[4]; //<------------------------Reserve memory
    mat3_ambient[0]        = 0.1f; //<----------------------------------A1r
    mat3_ambient[1]        = 0.1f; //<----------------------------------A1g
    mat3_ambient[2]        = 0.1f; //<----------------------------------A1b
    mat3_ambient[3]        = 1.0f; //<----------------------------------A1a
    
    mat3_shininess        = new GLfloat[1]; //<------------------------Reserve memory
    mat3_shininess[0]    = 60.0f; //<---------------------------------material 1 shininess
    //<-material 1 ends
    
    //->LIGHT 1 begins
    light3_position        = new GLfloat[4]; //<------------------------Reserve memory
    light3_position[0]    =  -15.0f; //<---------------------------------L1x
    light3_position[1]    =  1.0f; //<---------------------------------L1y
    light3_position[2]    =  10.0f; //<---------------------------------L1z
    light3_position[3]    =  0.0f; //<---------------------------------L1w
    
    light3_specular        = new GLfloat[4]; //<------------------------Reserve memory
    light3_specular[0]    = 1.0f; //<----------------------------------L1Sr
    light3_specular[1]    = 1.0f; //<----------------------------------L1Sg
    light3_specular[2]    = 1.0f; //<----------------------------------L1Sb
    light3_specular[3]    = 1.0f; //<----------------------------------L1Sa
    
    light3_diffuse        = new GLfloat[4]; //<------------------------Reserve memory
    light3_diffuse[0]    = 0.9f; //<----------------------------------L1Dr
    light3_diffuse[1]    = 0.9f; //<----------------------------------L1Dg
    light3_diffuse[2]    = 1.0f; //<----------------------------------L1Db
    light3_diffuse[3]    = 1.0f; //<----------------------------------L1Da
    
    light3_ambient        = new GLfloat[4]; //<------------------------Reserve memory
    light3_ambient[0]    = 0.1f; //<----------------------------------L1Ar
    light3_ambient[1]    = 0.1f; //<----------------------------------L1Ag
    light3_ambient[2]    = 0.1f; //<----------------------------------L1Ab
    light3_ambient[3]    = 1.0f; //<----------------------------------L1Aa
    //<-LIGHT 1 ends
    
    glLightfv( GL_LIGHT3, GL_POSITION,  light3_position );
    glLightfv( GL_LIGHT3, GL_AMBIENT,   light3_ambient);
    glLightfv( GL_LIGHT3, GL_DIFFUSE,   light3_diffuse);
    glLightfv( GL_LIGHT3, GL_SPECULAR,  light3_specular);
    
    mat2_specular        = new GLfloat[4];
    mat2_specular[0]    = 1.0f; //<----------------------------------S0r
    mat2_specular[1]    = 1.0f; //<----------------------------------S0g
    mat2_specular[2]    = 1.0f; //<----------------------------------S0b
    mat2_specular[3]    = 1.0f; //<----------------------------------S0a
    
    mat2_diffuse        = new GLfloat[4];
    mat2_diffuse[0]        = 1.0f; //<----------------------------------D0r
    mat2_diffuse[1]        = 1.0f; //<----------------------------------D0g
    mat2_diffuse[2]        = 1.0f; //<----------------------------------D0b
    mat2_diffuse[3]        = 1.0f; //<----------------------------------D0a
    
    mat2_shininess        = new GLfloat[1];
    mat2_shininess[0]    = 60.0f;
    
    light2_position        = new GLfloat[4];
    light2_position[0]    = 0.0f; //<----------------------------------L0x
    light2_position[1]    = 10.0f; //<----------------------------------L0y
    light2_position[2]    = 0.0f; //<----------------------------------L0z
    light2_position[3]    = 0.0f; //<----------------------------------L0w
    glLightfv( GL_LIGHT2, GL_POSITION,  light2_position );
    // Enable LIGHT 1:
    glEnable( GL_LIGHT2 );
    glEnable( GL_LIGHT1 );
    glEnable( GL_LIGHT3 );
    // Enable lighting:
    glEnable( GL_LIGHTING );
//     glEnable(GL_LIGHT0);
    // Enable depth test (distinguish between near and far faces):
    glEnable( GL_DEPTH_TEST );
    glEnable(GL_NORMALIZE);
    
}

void display()                            // Called for each frame (about 60 times per second).
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear color and depth buffers.
    glPushMatrix();{
        glMaterialfv( GL_FRONT,     GL_DIFFUSE, mat2_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat2_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat2_shininess    );
  
        glTranslatef( 1.5f, 0.0f, -0.8f );
        myField->draw();
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat1_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat1_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat1_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat1_shininess    );
        glRotatef(rot, 0, 1, 0);
        glTranslatef( transx, -2.50f, trans );
        glRotatef(rotx, 0, 1, 0);
        
        glScalef(2, 2, 2);
        myCar->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat1_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat1_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat1_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat1_shininess    );
        glTranslatef( transPx, -2.50f, -35.0f );
        glScalef(2, 2, 2);
        myCar2->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat1_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat1_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat1_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat1_shininess    );
        glTranslatef( -10.0f, -2.50f, -8.0f );
        glScalef(2, 2, 2);
        myCar3->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat1_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat1_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat1_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat1_shininess    );
        glTranslatef( 10.0f, -2.50f, -8.0f );
        glScalef(2, 2, 2);
        myCar4->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat3_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat3_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat3_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat3_shininess    );
        glTranslatef( trans2Px, -2.50f, 40.0f );
        glRotatef(180, 0, 1, 0);
        
//        glTranslatef( 0.0f, 0.0f, -26.0f );
        glColor3f(0, 1, 0);
        glScalef(2, 2, 2);
        
        myCar5->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat3_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat3_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat3_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat3_shininess    );
        glTranslatef( trans2x, -2.50f, trans2 );
        glRotatef(rot2, 0, 1, 0);
       
        glRotatef(rot2x, 0, 1, 0);
        glColor3f(0, 1, 0);
        glScalef(2, 2, 2);
        myCar6->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat3_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat3_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat3_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat3_shininess    );
        glRotatef(180, 0, 1, 0);
        glTranslatef( -10.0f, -2.50f, -12.0f );
         glColor3f(0, 1, 0);
        glScalef(2, 2, 2);
        myCar7->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv( GL_FRONT,  GL_AMBIENT,   mat3_ambient        );
        glMaterialfv( GL_FRONT,     GL_DIFFUSE,   mat3_diffuse        );
        glMaterialfv( GL_FRONT,  GL_SPECULAR,  mat3_specular    );
        glMaterialfv( GL_FRONT,  GL_SHININESS, mat3_shininess    );
        glRotatef(180, 0, 1, 0);
        glTranslatef( 10.0f, -2.50f, -12.0f );
         glColor3f(0, 1, 0);
        glScalef(2, 2, 2);
        myCar8->draw();
        
    }glPopMatrix();
    
    glPushMatrix();{
        glMaterialfv(GL_FRONT, GL_DIFFUSE, mat0_diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat0_specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat0_shininess);
        glColor3f(1, 1, 0);
         glTranslatef( ballpx, ballpy, ballpz);
        
        glScalef(0.06, 0.06, 0.06);
        myBall->draw();
    }glPopMatrix();
    
    displayText( 5, 20, bufferFPS );// Swap the hidden and visible buffers.
    inCollision();
   // inCollision2();
    glutSwapBuffers();
   
}
void inCollision(){
    
    float dx = transx - ballpx;
    float dy = -2.5 - ballpy;
    float dz = trans - ballpz;
    
    float ds = (float)sqrt( dx * dx + dy * dy + dz * dz );
    
    
    //    vector3f v1=vector3f(pos[0],pos[1],pos[2]);
    //    vector3f v2=vector3f(other->pos[0],other->pos[1],other->pos[2]);
    //
    //    float ds=v.distance(vector3f(pos[0],pos[1],pos[2]),vector3f(other->pos[0],other->pos[1],other->pos[2]));
    
    float d2x = trans2x - ballpx;
    float d2y = -2.5 - ballpy;
    float d2z = trans2 - ballpz;
    
    float d2px = trans2Px - ballpx;
    float d2py = -2.5 - ballpy;
    float d2pz = 40 - ballpz;
    
    float ds2 = (float)sqrt( d2x * d2x + d2y * d2y + d2z * d2z );
    
    float dsp2 = (float)sqrt( d2px * d2px + d2py * d2py + d2pz * d2pz );
    
    float dpx = transPx - ballpx;
    float dpy = -2.5 - ballpy;
    float dpz = -35 - ballpz;
    
    float dsp = (float)sqrt( dpx * dpx + dpy * dpy + dpz * dpz );
    if((dsp)< (1+myBall->radius))
    {
        
        printf("Entre");
        ballpz+= 10;
    }
    
    if((ds)< (1+myBall->radius)&&myCar->right->shoot==false)
    {
        if(rotx==180){
            ballpz-= 10 ;
        }
        if(rotx==0){
            ballpz+= 10 ;
        }
        if(rotx==90){
            ballpx+= 10;
        }
        if(rotx==270){
            ballpx-= 10;
        }
       
    }
    if((ds2)< (1+myBall->radius)&& myCar6->right->shoot==false)
    {
        
        //printf("Entre");
        if(rot2x==180){
            ballpz+= 10;
        }
        if(rot2x==0){
            ballpz-= 10;
        }
        if(rot2x==90){
            ballpx-= 10;
        }
        if(rot2x==270){
            ballpx+= 10;
        }
    }
    
    if((dsp2)< (1+myBall->radius))
    {
        
        //printf("Entre");
        ballpz-= 10;
    }
    
    
    
    if((ds2)< (1+myBall->radius))
    {
        
        //printf("Entre");
        if(rot2x==180){
            ballpz+= rand() % 2 ;
        }
        if(rot2x==0){
            ballpz-= rand() % 2 ;
        }
        if(rot2x==90){
            ballpx-= rand() % 2 ;
        }
        if(rot2x==270){
            ballpx+= rand() % 2 ;
        }
    }
    
    if((ds)< (1+myBall->radius))
    {
        
        //printf("Entre");
        if(rotx==180){
             ballpz-= rand() % 2 ;
        }
        if(rotx==0){
            ballpz+= rand() % 2 ;
        }
        if(rotx==90){
             ballpx+= rand() % 2 ;
        }
        if(rotx==270){
            ballpx-= rand() % 2 ;
        }
    }
       
}


void idle()                                                            // Called when drawing is finished.
{
    if (portero){
    transPx+= rand() % 2 ;
    }else{
      transPx-= rand() % 2;
    }
    
    if(transPx>15.0f){
        portero=false;
    }
    
    if(transPx<-15.0f){
        portero=true;
    }
    
    if (portero2){
        trans2Px+= rand() % 2 ;
    }else{
        trans2Px-= rand() % 2;
    }
    
    if(trans2Px>15.0f){
        portero2=false;
    }
    
    if(trans2Px<-15.0f){
        portero2=true;
    }
    frames++;
    time = glutGet( GLUT_ELAPSED_TIME );
//    sprintf( bufferFPS, "Green - %4i vs%5i - Blue\n", marcador,marcador2);
    if(reset){
        marcador=0;
        marcador2=0;
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        trans=-8.0f;
        transx=0.0f;
        transPx=0.0f;
        portero= true;
        portero2=true;
        rotx=0.0f;
        trans2= 12.0f;
        trans2x=0.0f;
        trans2Px=0.0f;
        rot2x=0.0f;
        rot=0.0f;
        rot2=180.0f;
        reset=false;
        
        
        ballpx=0.0f;
        ballpy=-3.4f;
        ballpz=3.2f;
        check=false;
        
    }
    
    if(check==true){
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        trans=-8.0f;
        transx=0.0f;
        transPx=0.0f;
        portero= true;
        portero2=true;
        rotx=0.0f;
        trans2= 12.0f;
        trans2x=0.0f;
        trans2Px=0.0f;
        rot2x=0.0f;
        rot=0.0f;
        rot2=180.0f;
        
        
        ballpx=0.0f;
        ballpy=-3.4f;
        ballpz=3.2f;
        check=false;
    }
    if( ballpx>= -4.0f && ballpx<=5.0f && ballpz>=40.5f)
    {
        check=false;
        fps = frames * 1000.0f / (time - timebase);
        fps = 0;
        marcador2=marcador2+1;
        sprintf( bufferFPS, "Green - %4i vs%5i - Blue\n", marcador,marcador2);
//        ballpx=0.0f;
//        ballpz=4.0f;
        trans=-8.0f;
        transx=0.0f;
        transPx=0.0f;
        portero= true;
        portero2=true;
        rotx=0.0f;
        trans2= 12.0f;
        trans2x=0.0f;
        trans2Px=0.0f;
        rot2x=0.0f;
        rot=0.0f;
        rot2=180.0f;
        
        
        ballpx=0.0f;
        ballpy=-3.4f;
        ballpz=3.2f;
        timebase = time;
        frames = 0;
    }
    if( ballpx>= -4.0f && ballpx<=5.0f && ballpz<=-35.5f)
    {
        fps = frames * 1000.0f / (time - timebase);
        fps = 0;
        marcador=marcador+1;
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        trans=-8.0f;
        transx=0.0f;
        transPx=0.0f;
        portero= true;
        portero2=true;
        rotx=0.0f;
        trans2= 12.0f;
        trans2x=0.0f;
        trans2Px=0.0f;
        rot2x=0.0f;
        rot=0.0f;
        rot2=180.0f;
        
        
        ballpx=0.0f;
        ballpy=-3.4f;
        ballpz=3.2f;
        timebase = time;
        frames = 0;
    }
    
    if( (ballpx<-4.0f || ballpx>5.0f) && ballpz<-35.5f){
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        ballpx= 0.0f;
        ballpz= -20.0f;
        trans2= -8.0f;
        timebase = time;
        frames = 0;
    }
    if( (ballpx<-4.0f || ballpx>5.0f) && ballpz > 40.5f){
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        ballpx= 0.0f;
        ballpz= 20.0f;
        trans= -8.0f;
        timebase = time;
        frames = 0;
    }
    
    if(trans>50.0f||trans<-43.0f||transx>24.0f||transx<-18.0f){
         sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        trans=-8.0f;
        transx=0.0f;
    }
    
    if(trans2>49.0f||trans2<-48.0f||trans2x>24.0f||trans2x<-18.0f){
        sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        trans2=8.0f;
        trans2x=0.0f;
    }
    
    if(ballpx>24.0f||ballpx<-18.0f){
         sprintf( bufferFPS, "Green - %4i vs %5i - Blue\n", marcador,marcador2);
        ballpx=0.0f;
        ballpx=-3.4f;
        ballpz=3.2f;
    }
    glutPostRedisplay();// Display again.
   
}

void RecieveKeys(unsigned char key, int k, int y){
    switch(key){
        case 'w':
            rot=0;
            rotx=0;
            trans+=1.0f;
            
//            printf("%f",trans);
            break;
        case 's':
            
            rot=0;
            rotx=180;
           trans-=1.0f;
//            printf("%f",trans);
            break;
        case 'd':
            rot=0;
            rotx=90;
            transx+=1.0f;
//            printf("%f",transx);
            break;
        case 'a':
            rot=0;
            rotx=270;
//        printf("%f",transx);

            
            transx-=1.0f;
            break;
        case 'v':
        reset=true;
        break;
        case 'f':
        exit(0);
        break;
        case 'q':
            
            myCar->right->shoot=!myCar->right->shoot;
//            printf("%b",myCar->right->shoot);
            break;
            
        case 'l':
            rot2=180;
             rot2x=270;
            trans2x+=1.0f;
        
            break;
        case 'j':
            rot2=180;
             rot2x=90;
            trans2x-=1.0f;
            break;
        case 'i':
            rot2=180;
            rot2x=0;
            trans2-=1.0f;
//         printf("%f",trans2);
            break;
        case 'k':
            rot2=180;
            rot2x=180;
            trans2+=1.0f;
            break;
        case 'u':
            
            myCar6->right->shoot=!myCar6->right->shoot;
            //            printf("%b",myCar->right->shoot);
            break;
        default:
            break;
    }
}
void reshape(int x, int y)                                            // Called when the window geometry changes.
{
    
    glMatrixMode(GL_PROJECTION);     // 2D                                // Go to 2D mode.
    glLoadIdentity();                                                // Reset the 2D matrix.
    gluPerspective(70.0, (GLdouble)x / (GLdouble)y, 0.5, 200.0);        // Configure the camera lens aperture.
    glMatrixMode(GL_MODELVIEW); // 3D                                        // Go to 3D mode.
    glViewport(0, 0, x, y);                                            // Configure the camera frame dimensions.
    gluLookAt(-8.0, 45, 90.0,
        0.0, 0.0, 0.0,
        0.0, 1.0, 0.0);
    display();
}

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);                                            // Init GLUT with command line parameters.
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );        // Use 2 buffers (hidden and visible). Use the depth buffer. Use 3 color channels.
    glutInitWindowSize(800, 800);
    glutCreateWindow(argv[0]);

    init();
    glutReshapeFunc(reshape);                                        // Reshape CALLBACK function.
    glutDisplayFunc(display);
    glutKeyboardFunc(RecieveKeys); // Display CALLBACK function.
    glutIdleFunc(idle);                                                // Idle CALLBACK function.

    glutMainLoop();                                                    // Begin graphics program.
    return 0;                                                        // ANSI C requires a return value.
}

void displayText( int x, int y, char* txt )
{
    GLboolean lighting;
    GLint viewportCoords[4];
    glColor3f( 0.0, 1.0, 0.0 );
    glGetBooleanv( GL_LIGHTING, &lighting       );
    glGetIntegerv( GL_VIEWPORT, viewportCoords );
    if( lighting )
    {
        glDisable( GL_LIGHTING );
    }
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D( 0.0, viewportCoords[2], 0.0, viewportCoords[3] );
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i( x, viewportCoords[3] - y );
    while( *txt )
    {
        glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18, *txt );
        txt++;
    }
    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
    
    if( lighting )
    {
        glEnable( GL_LIGHTING );
    }
}
