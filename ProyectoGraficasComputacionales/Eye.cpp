//
//  Eye.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include <stdio.h>

#include "Eye.h"


Eye::Eye()
{
    
}

Eye::~Eye()
{
}

void Eye::draw()
{
    glPushMatrix();
    {
        glColor3f(1, 1, 1);
        glScalef(0.1f,0.1f,0.1f);
        glutSolidCube(1.0);
    }
    glPopMatrix();
}
