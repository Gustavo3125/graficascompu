//
//  Field.cpp
//  ProyectoGraficasComputacionales
//
//  Created by Gustavo Méndez on 06/11/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include "Field.h"

GLMmodel*    field;

Field::Field()
{
   glShadeModel(GL_SMOOTH);
    field = glmReadOBJ("/Users/gustavomendez/Documents/Universidad ITC/Septimo Semestre/Graficas/OpenGL Graficas/ProyectoGraficasComputacionales/ProyectoGraficasComputacionales/soccerStadium.obj");
//    field = glmReadOBJ("/Users/DanielaMartin/graficascompu/ProyectoGraficasComputacionales/soccerStadium.obj");

    // When the model is too big make it fit inside a 1x1x1 cube:
    glmUnitize(field);
    glmScale(field, 50.0f);

    // When the model doesn't have normals, glm can add them:
    glmFacetNormals(field); // and then per face
    glmVertexNormals(field, 4.0f); // first per vertex
   


}

Field::~Field()
{
}

void Field::draw()
{
    glPushMatrix();
    {
        glTranslatef(0,11.8f,0.5);
        glTranslatef(0.0f, 0, 3.5f);
       // glTranslatef(-20.0f, 0, 0.0f);
        glRotated(90, 0, 1, 0);
        
        glmDraw(field, GLM_SMOOTH | GLM_TEXTURE);
    }
    glPopMatrix();



}

