//
//  Field.hpp
//  ProyectoGraficasComputacionales
//
//  Created by Gustavo Méndez on 06/11/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif
#include "glm.h"
#include "cTexture.h"

//#ifndef __FIELD
//#define __FIELD
//
//class Field
//{
//public:
//    Field    (    float    side,
//             bool    use_mipmaps    );
//    ~Field    (    void                );
//
//    void    draw    (    void                );
//
//private:
//    float    side;
//    float    hside;
//    Texture targas[6];
//};
//
//#endif __FIELD


class Field
{
public:
    Field();    //Constructor
    ~Field();   //Destroyer

    void draw();

};

