#include "ForeArm.h"

ForeArm::ForeArm()
{
	
}

ForeArm::~ForeArm()
{
}

void ForeArm::draw()
{
	glPushMatrix();
	{
        glColor3f(0.0f, 1.0f, 1.0f);
        glScalef(0.4f,0.7f,0.4f);

		glutSolidCube(1.0);
	}
	glPopMatrix();
}
