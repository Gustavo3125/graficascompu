#include "Hand.h"

Hand::Hand()
{
	
}

Hand::~Hand()
{
}

void Hand::draw()
{
	glPushMatrix();
	{
        glTranslatef(0, 0, -0.1);
        glColor3f(0, 1, 1);
        glScalef(0.2f,0.7f,0.2f);

		glutSolidCube(1.0);
	}
	glPopMatrix();
    
    glPushMatrix();
    {
        glTranslatef(0, -0.11, 0.17);
        glColor3f(0, 1, 1);
        glScalef(0.15f,0.15f,0.15f);
        
        glutSolidCube(1.0);
    }
    glPopMatrix();
}
