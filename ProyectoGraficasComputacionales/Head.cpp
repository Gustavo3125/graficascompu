//
//  Head.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include "Head.h"

Head::Head()
{
    right = new Blasters();
    left= new Blasters();
    middle= new Blasters();
    radius = 0.6f;
    rot=0.0f;
    bRot=true;
}

Head::~Head()
{
    
}

void Head::draw()
{
    
    glPushMatrix();
    {
        glPushMatrix();
        {
            glRotatef(rot,0,1,0);
            
            glPushMatrix();{
                glScalef(0.5, 0.5, 0.5);
                glColor3f(0, 1.0f, 1.0f);
                glutSolidSphere(radius, 20, 20);
            }glPopMatrix();
            
            
        }
        glPopMatrix();
   
        
    }
    glPopMatrix();
    
    if(rot>20){
        bRot=false;
        
    }
    if(rot<-20){
        bRot=true;
        
    }
    
    if(bRot==true){
        rot+=0.588f;
    }else{
        rot-=0.588f;
    }
    
}
