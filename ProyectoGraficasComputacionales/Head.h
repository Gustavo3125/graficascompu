//
//  Head.h
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif
#include "Blasters.h"
#include "Eye.h"

class Head
{
public:
    Head();    //Constructor
    ~Head();   //Destroyer
    
    void draw();
    float radius;
    Blasters* right;
    Blasters* left;
    Blasters* middle;
    Eye* leftEye;
    Eye* rightEye;
    float rot;
    bool bRot;
};

