#pragma once
#ifdef __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
	#include <math.h>
#else
	#include "freeglut.h"
	#include <stdio.h>
	#include <math.h>
#endif

class Knee
{
	public:
	Knee();    //Constructor
	~Knee();   //Destroyer

	void draw();

	float radius;
};

