//
//  AT-ST.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//


#include "Player.h"




Player::Player()
{

    right = new Leg();
    left= new Leg();
    body = new Body();
    head=new Head();
    rightArm = new Arm();
    leftArm = new Arm();
    rightHand = new Hand();
    left->SetZRot(70.0f);
    left->kRot=70.0f;
    rot=0.0f;
    bRot=true;
    
}

Player::~Player()
{
    
}

void Player::draw()
{
   
    glPushMatrix();
    {
        glPushMatrix();
        {
            body->draw();
            
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(0,1.2f,0);
//            glRotatef(rot,0,1,0);
            
            head->draw();
            
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(-0.3,-0.36f,0);
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            
            left->draw();
            
        }
        glPopMatrix();
        glPushMatrix();
        {
            glTranslatef(0.3,-0.36f,0);
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            right->draw();
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(0.53,0.5f,0);
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            rightArm->draw();
        }
        glPopMatrix();
        glPushMatrix();
        {
            glTranslatef(1.4f,1.0f,0);
            glRotatef(rot,0,1,0);
            glScalef(0.5, 0.5, 0.5);
            
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            rightHand->draw();
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(-0.53,0.5f,0);
            //glScalef(0.5, 0.5, 0.5);
            glRotatef(180,0,1,0);
            leftArm->draw();
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(-1.4f,1.0f,0);
            glRotatef(rot,0,1,0);
            glScalef(0.5, 0.5, 0.5);
            
            //glScalef(0.5, 0.5, 0.5);
            //glRotatef(zRot,0,0,1);
            leftHand->draw();
        }
        glPopMatrix();
        
        
        
        
        
    }
    glPopMatrix();
    
    if(rot>20){
        bRot=false;
        
    }
    if(rot<-20){
        bRot=true;
        
    }
    
    if(bRot==true){
        rot+=0.588f;
    }else{
        rot-=0.588f;
    }

}
