//
//  AT-ST.h
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif

#include "cLeg.h"
#include "cBody.h"
#include "Head.h"
#include "Arm.h"
#include "Hand.h"
class Player
{
public:
    Player();
    ~Player();
    void draw();
    
    // Instance these in the constructor:
    Arm* rightArm;
    Arm* leftArm;
    Leg* right;
    Leg* left;
    Head* head;
    Body* body;
    Hand* rightHand;
    Hand* leftHand;
    float zRot;
    float rot;
    float fRot;
    float kRot;
    float aRot;
    bool bZRot;
    bool bARot;
    bool bRot;
    
    
};

