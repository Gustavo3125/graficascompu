//
//  Shoulder.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 07/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include <stdio.h>

#include "Shoulder.h"

Shoulder::Shoulder()
{
    
}

Shoulder::~Shoulder()
{
}

void Shoulder::draw()
{
    glPushMatrix();
    {
        glColor3f(0, 1, 1);
        quadratic = gluNewQuadric();
        glRotatef(90,0.0f,1.0f,0.0f);
        gluCylinder(quadratic,0.1f,0.1f,0.2f,16,16);
    }
    glPopMatrix();
}

