#include "Tricep.h"

Tricep::Tricep()
{
	
}

Tricep::~Tricep()
{
}

void Tricep::draw()
{
	glPushMatrix();
	{
        glColor3f(0.0f, 1.0f, 1.0f);
        glScalef(0.5f,0.4f,0.7f);

		glutSolidCube(1.0);
	}
	glPopMatrix();
}
