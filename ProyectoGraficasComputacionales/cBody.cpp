//
//  cBody.cpp
//  AT-ST
//
//  Created by Gustavo Méndez on 01/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#include "cBody.h"




Body::Body()
{
    n=false;
    right = new Leg();
    left= new Leg();
    left->SetZRot(70.0f);
    left->kRot=70.0f;
    rot=0.0f;
    bRot=true;
}

Body::~Body()
{
    
}

void Body::draw()
{
    
    glPushMatrix();
    {
        glPushMatrix();
        {
            glColor3f(0.0f, 1.0f, 1.0f);
            glRotatef(rot,0,1,0);

            glScalef(1, 0.2, 0.4);
            glutSolidCube(1.0);
            
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glColor3f(0.0f, 1.0f, 1.0f);
            glTranslatef(0,0.45f,0);
            glRotatef(rot,0,1,0);
            
            glScalef(1.05, 0.65, 0.6);
            glutSolidCube(1.0);
            
        }
        glPopMatrix();
        
        glPushMatrix();
        {
             glColor3f(0.0f, 1.0f, 1.0f);
            glTranslatef(0,0.95f,0);
            glRotatef(rot,0,1,0);
            
            glScalef(0.15, 0.3, 0.15);
            glutSolidCube(1.0);
            
        }
        glPopMatrix();
 
        
      
        
        
    }
    glPopMatrix();
    
    if(rot>20){
        bRot=false;
        
    }
    if(rot<-20){
        bRot=true;
        
    }
    
    if(bRot==true){
        rot+=0.588f;
    }else{
        rot-=0.588f;
    }
    
}
