//
//  cBody.h
//  AT-ST
//
//  Created by Gustavo Méndez on 01/09/17.
//  Copyright © 2017 Gustavo Méndez. All rights reserved.
//

#pragma once
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>
#else
#include "freeglut.h"
#include <stdio.h>
#include <math.h>
#endif
#include "cLeg.h"
class Body
{
public:
    Body();    //Constructor
    ~Body();   //Destroyer
    
    void draw();
    bool n;
    Leg* right;
    Leg* left;
    float rot;
    bool bRot;
};

