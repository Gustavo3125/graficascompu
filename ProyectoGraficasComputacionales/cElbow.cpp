#include "cElbow.h"

Elbow::Elbow()
{
	radius = 0.6f;
}

Elbow::~Elbow()
{
}

void Elbow::draw()
{
	glPushMatrix();
	{
        glColor3f(0, 1, 1);
        glScalef(0.2f,0.2f,0.2f);
		glutSolidSphere(radius, 20, 20);
	}
	glPopMatrix();
}
