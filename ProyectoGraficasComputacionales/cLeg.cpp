#include "cLeg.h"


Leg::Leg()
{
	foot = new Wheel();
    ankle = new Ankle();
    knee = new Knee();
    thigh = new Thigh;
	zRot = 0.0f;
    kRot=0.0f;
    aRot=0.0f;
    bZRot=true;
    shoot=true;
}

Leg::~Leg()
{
    
}

void Leg::SetZRot(float num){
    zRot=num;
}


void Leg::SetARot(float num){
    aRot=num;
}
void Leg::draw()
{
     
	glPushMatrix();
	{
        if(shoot==true){
		glPushMatrix();
		{
            glTranslatef(0, 0.25f, 0.0f);
            glRotatef(-aRot,1,0,0);
            glTranslatef(0, -0.25f, 0.0f);
			
            thigh->draw();
		}
		glPopMatrix();

        }else{
            glTranslatef(0, 0.25f, 0.0f);
            glRotatef(zRot,1,0,0);
            glTranslatef(0, -0.25f, 0.0f);
            
            thigh->draw();
        }
		glPushMatrix();
		{
            
            
            glTranslatef(0,-0.3f,0);
            
            glTranslatef(0, 0.25f, 0.0f);
            glRotatef(-kRot,1,0,0);
            glTranslatef(0, -0.25f, 0.0f);
			//glRotatef(zRot,0,0,1);
			knee->draw();
		}
		glPopMatrix();
        
        glPushMatrix();
        {
            
            glTranslatef(0,-0.55f,0);
            glTranslatef(0, 0.45f, 0.0f);
            glRotatef(zRot,1,0,0);
            glTranslatef(0, -0.45f, 0.0f);
            //glRotatef(zRot,0,0,1);
            ankle->draw();
        }
        glPopMatrix();
        
        glPushMatrix();
        {
            glTranslatef(0,-0.8f,0);
            
            glTranslatef(0, 0.35f, 0.0f);
            glRotatef(zRot,1,0,0);
            glTranslatef(0, -0.35f, 0.0f);
            glColor3f(0,1,1);
            //glRotatef(zRot,0,0,1);
            foot->draw();
        }
        glPopMatrix();

		
	}
	glPopMatrix();
    if(shoot==true){
    if(zRot>70){
        bZRot=false;
        
    }
    if(zRot<0){
        bZRot=true;
        
    }
    }else{
        if(zRot>70){
            bZRot=false;
            
        }
        if(zRot<-30){
            bZRot=true;
            
        }
    }
    
    if(bZRot==true){
       zRot += 1.0f;
        aRot+=1.0f;
        kRot+=1.0f;
    }else{
         zRot -= 1.0f;
        kRot-=1.0f;
        aRot-=1.0f;
        
    }
  
    
    
}
