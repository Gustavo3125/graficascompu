#pragma once
#ifdef __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
	#include <math.h>
#else
	#include "freeglut.h"
	#include <stdio.h>
	#include <math.h>
#endif

#include "cWheel.h"
#include "Ankle.h"
#include "Thigh.h"
#include "Knee.h"

class Leg
{
public:
	Leg();
	~Leg();
    void SetZRot(float);
    void SetARot(float);
	void draw();

	// Instance these in the constructor:
	Wheel* foot;
    Ankle* ankle;
    Knee* knee;
    Thigh* thigh;
	float zRot;
    float fRot;
    float kRot;
    float aRot;
    bool bZRot;
    bool bARot;
    bool shoot;
    
    
};

